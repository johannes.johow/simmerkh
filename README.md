Considering a scenario with inheritable wealth under local resource competition consanguineous marriages may be adaptive (in terms of fitness maximizing) by preventing family property from fragmentation in future generations. This package provides functionality for performing Discrete Event Simulations of births, marriages and deaths in a synthetic population which consists of aging males and females with specific time-varying probabilities of death, marriage, and reproduction depending on limited resources as the availability of a spouse and the provision of a family’s livelihood.
Trajectory I: Individual life histories
Beginning from birth, individuals wander their path through the possible states in their life history depicted in Fig. 1.
Birth. This initial event is triggered from the generating marriage (fanily-of-origin) being modeled in trajectory II. If a birth occurs, individual attributes are set for sex (randomly assigned), wealth (a numerical value inherited from the fanily-of-origin and 1st year survival. The latter attributes determines whether the individual will die before or after entering the next state.
1st birthday. After one year, individual survival status will no longer inhibit the generating family of producing a younger sibling - although interbirth interval will be usually longer).
15th birthday. After 15 years, individuals may enter the marriage market - although they will only be able to marry, if they can find an available (unmarried) spouse with opposite-sex.
Marriage. This event triggers trajectory II and may result in new births (i.e. a subsequent generation).
Death. This event will deactivate trajectory II. For simplicity, widowed partners do neither reenter the marriage market nor affect the survival of existing offspring (although these consequences may be implemented later)

Trajectory II: Marriages (i.e. reproductive unions generating arrivals in Trajectory I, namely ‘births’ of individuals)
