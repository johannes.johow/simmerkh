#' Get survival time in years
#'
#' @param is_male numeric (0 = female, 1 = male)
#' @param is_farmer numeric (0 = landless, 1 = farmer)
#' @param n number of times to return
#' @param min_time Minimum age at death
#' @param max_time Maximum age at death
#'
#'
#' @return numeric (Age at death in years)
#' @export
#'
#' @examples
#' get_survival()
get_survival <- function(is_male = 1,
                         is_farmer = 0,
                         n = 1,
                         min_time =0,
                         max_time =119) {
  survout <- survival::survfit(simmerkh:::kh_survival,
                               newdata =
                                 data.frame(male = is_male, farm = is_farmer))
  sample(survout$time[
    survout$time>=min_time &
      survout$time<=max_time
  ],
         size = n,
         prob = survout$cumhaz[
           survout$time>=min_time &
             survout$time<=max_time
           ] / sum(survout$cumhaz[
             survout$time>=min_time &
               survout$time<=max_time
             ]))
}
