# Overview on writing (TBD: and reading) of attributes over trajectories

## Setting of attributes (always as numeric)
## t1: Infancy
- bdate: simulation time at birth via `simmer::now()`
- is_male: 0=female, 1=male  (via `simmerkh::is_male()`)
- grasen: (zero-inflated) amount of landholding (via `simmerkh::get_grasen()`)
- parity: initially 0 (for counting future rity progression status)
- is_farmer: 0=no farmer, 1=farmer (if `grasen>74`) 
- surv_to_1y: 0=not surviving 1st year, 1=1st year survivor (via `simmerkh::surv_to_1y()`)
- state_duration: if `surv_to_1y=1`set to 1 else the length of the year at a randomly drawn day (via `sample()`)

## t2: Childhood
- surv_to_15y: 0=not surviving 15th year, 1=15th year survivor (via `simmerkh::surv_to_15y()`)
- state_duration: if `surv_to_15y=1`set to 14 else a random draw of numbers 0-13 (via `sample()`)

## t3: Adulthood
- aam: (Hypothetical) age at marriage (see below; set via `simmerkh::get_age_at_marriage()`)
- ever_married: Permits marriage if `aam<aad` (TBD: and a mate is available in queue!)

## t4: Marriage
- married_to
- time_to_death (first time (explicitly) defined??)
- ceb
- proto_int
